#include "threads.h"

#include <thread>
#include <math.h>
#include <mutex>

std::mutex fileLocker;


void I_Love_Threads()
{
	std::cout << "I Love Threads" << std::endl;
}

void call_I_Love_Threads()
{
	std::thread t(I_Love_Threads);
	t.join();
}

void printVector(const std::vector<int>& primes)
{
	for (int i : primes)
		std::cout << i << std::endl;
}

void getPrimes(const int begin, const int end, std::vector<int>& primes)
{
	bool flag = false;

	for (int i = begin; i < end; i++)
	{
		// check if the number is prime
		flag = true;
		for (int j = 2; j <= sqrt(i); j++)
		{
			if (i % j == 0)
			{
				flag = false;
				break; // stop the first loop
			}
		}

		if(flag)
			primes.emplace_back(i);
	}
}

std::vector<int> callGetPrimes(const int begin, const int end)
{
	std::vector<int> primes;
	const clock_t begin_time = clock(); // Initilaze the clock

	std::thread th(getPrimes, begin, end, ref(primes));
	th.join();

	std::cout << float(clock() - begin_time) / CLOCKS_PER_SEC << std::endl; // calculate the time difference
	return primes;
}

void writePrimesToFile(int begin, int end, std::ofstream& file)
{
	std::vector<int> primes = callGetPrimes(begin, end);

	for (int i : primes)
	{
		fileLocker.lock();
		file << std::to_string(i) << std::endl;
		fileLocker.unlock();
	}
		
}

void callWritePrimesMultipleThreads(int begin, int end, const std::string& filePath, int N)
{
	std::ofstream file(filePath);

	const int diffN = end / N; // get the difference betwwen the parts (size of the diff is the total size seperated to N parts)

	int start = begin; // start equal to the start of the range in the first time
	
	int endOfN = start + diffN - 1; // end of the part equal to the start + the diff, sub one because it in the next part

	std::vector<std::thread> threadVector;

	const clock_t begin_time = clock(); // Initilaze the clock


	for (int i = 0; i < N; i++)
	{
		threadVector.emplace_back(writePrimesToFile, start, endOfN, ref(file));

		start += diffN;
		end += diffN;
	}

	for (int i = 0; i < threadVector.size(); i++)
		threadVector[i].join();

	file.close();

	std::cout << float(clock() - begin_time) / CLOCKS_PER_SEC << std::endl; // calculate the time difference

}
