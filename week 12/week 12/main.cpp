#include <thread>
#include "MessageSender.h"

int main()
{
	MessageSender* m = new MessageSender(); // from some reason i can't create this varible as static
	
	std::thread t(&MessageSender::readAdminFile, m);
	std::thread t2(&MessageSender::writeMessagesToUsersFile, m);
	
	t.detach();
	t2.detach();

	m->showMenu();
	delete m;


	return 0;
}
