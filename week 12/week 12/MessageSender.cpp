#include "MessageSender.h"

#include <iostream>
#include <fstream>
#include <chrono>
#include <thread>

MessageSender::MessageSender()
{

}

MessageSender::~MessageSender()
{

}

void MessageSender::showMenu()
{
	char userChoice;
	std::string name;
	bool flag = true;

	while (flag)
	{
		printMenu();
		userChoice = getchar();
		getchar(); //clear the buffer.

		switch (userChoice)
		{
		case SIGNIN:
			std::cout << "Enter name: ";
			std::getline(std::cin, name);

			insertUser(name);
			break;

		case SIGNOUT:
			std::cout << "Enter name: ";
			std::getline(std::cin, name);

			removeUser(name);
			break;

		case CONNECTED_USERS:
			printConnectedUsers();
			break;

		case EXIT:
			flag = false;
			break;

		default:
			std::cerr << "ERROR: Invalid option!" << std::endl;
			break;
		}
		system("pause");
	}
}

void MessageSender::readAdminFile()
{
	std::ifstream f;
	std::ofstream clearFile;
		
	std::string line;

	while (true)
	{
		std::this_thread::sleep_for(std::chrono::seconds(WAIT_TIME));

		f.open(DATA_PATH);

		if (f.fail())
		{
			std::cerr << "ERROR: Can't open " << DATA_PATH << std::endl;
			return;
		}

		{
			std::lock_guard<std::mutex> locker(_messageQueueLocker);
			while (std::getline(f, line))
			{
				_messageQueue.push(line);
			}
			f.close();
		}
		
		_syncSystem.notify_one();
		f.close();
		
		clearFile.open(DATA_PATH);
		clearFile << "";
		clearFile.close();
	}
}

void MessageSender::writeMessagesToUsersFile()
{
	std::ofstream outputFile;
	std::string* allMessages;
	std::unique_lock<std::mutex> msgLock(_messageQueueLocker);

	int queueLenght = 0;

	while (true)
	{
		_syncSystem.wait(msgLock);

		queueLenght = _messageQueue.size();
		allMessages = new std::string[queueLenght];

		for (int i = 0; !_messageQueue.empty(); i++)
		{
			allMessages[i] = _messageQueue.front();
			_messageQueue.pop();
		}

		msgLock.unlock();

		outputFile.open(OUTPUT_PATH);
		if (outputFile.fail())
		{
			std::cerr << "ERROR: Can't open " << OUTPUT_PATH << std::endl;
			return;
		}

		{
			std::lock_guard<std::mutex> usersLock(_usersLocker);
			for (int i = 0; i < queueLenght; ++i)
				for (auto& u : _connectedUsers)
					outputFile << u << ": " << allMessages[i] << std::endl;
		}

		delete[] allMessages;
		outputFile.close();
	}
	
}

void MessageSender::printMenu()
{
	system("cls");
	std::cout << "1. Sign In" << std::endl;
	std::cout << "2. Sign Out" << std::endl;
	std::cout << "3. Connected Users" << std::endl;
	std::cout << "4. Exit" << std::endl;
}

bool MessageSender::userExist(const std::string& user)
{
	std::lock_guard<std::mutex> l(_usersLocker);

	return _connectedUsers.find(user) != _connectedUsers.end();
}

void MessageSender::printConnectedUsers()
{
	std::lock_guard<std::mutex> l(_usersLocker);
	for (const std::string& usr : _connectedUsers)
		std::cout << usr << std::endl;
}

void MessageSender::insertUser(const std::string& user)
{
	if (userExist(user))
	{
		std::cerr << "ERROR: User is already exist!" << std::endl;
		return;
	}
	std::lock_guard<std::mutex> l(_usersLocker);
	_connectedUsers.insert(user);
}

void MessageSender::removeUser(const std::string& user)
{
	if (!userExist(user))
	{
		std::cerr << "ERROR: User is not exist!" << std::endl;
		return;
	}
	std::lock_guard<std::mutex> l(_usersLocker);
	_connectedUsers.erase(user);
}
