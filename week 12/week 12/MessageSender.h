#pragma once

#include <set>
#include <queue>
#include <string>
#include <mutex>

#define DATA_PATH "data.txt"
#define OUTPUT_PATH "output.txt"
#define WAIT_TIME 20

enum options
{
	SIGNIN = '1',
	SIGNOUT,
	CONNECTED_USERS,
	EXIT
};




class MessageSender
{
public:
	MessageSender();
	~MessageSender();
	void showMenu();
	void readAdminFile();
	void writeMessagesToUsersFile();

protected:
	void printMenu();
	bool userExist(const std::string& user);
	void printConnectedUsers();
	void insertUser(const std::string& user);
	void removeUser(const std::string& user);


private:
	std::set<std::string> _connectedUsers;
	std::queue<std::string> _messageQueue;

	std::mutex _usersLocker;
	std::mutex _messageQueueLocker;

	std::condition_variable _syncSystem;

};

